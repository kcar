manifest = File.exist?('.manifest') ?
  IO.readlines('.manifest').map!(&:chomp!) : `git ls-files`.split("\n")

Gem::Specification.new do |s|
  s.name = %q{kcar}
  s.version = (ENV['VERSION'] || '0.7.0').dup
  s.homepage = 'https://yhbt.net/kcar/'
  s.authors = ["kcar hackers"]
  s.description = File.read('README').split("\n\n")[1]
  s.email = %q{kcar-public@yhbt.net}
  s.extra_rdoc_files = IO.readlines('.document').map!(&:chomp!).keep_if do |f|
    File.exist?(f)
  end
  s.files = manifest
  s.summary = 'bytestream to Rack response converter'
  s.test_files = Dir['test/test_*.rb']
  s.extensions = %w(ext/kcar/extconf.rb)
  s.add_development_dependency('test-unit', '~> 3.0')

  # Note: To avoid ambiguity, we intentionally avoid the SPDX-compatible
  # 'Ruby' for the Ruby 1.8 license.  This is because Ruby 1.9.3 switched
  # to BSD-2-Clause, but we inherited our license from Mongrel when
  # Ruby was at 1.8.  We cannot automatically switch licenses when Ruby
  # changes; so we maintain the Ruby 1.8 license (not 1.9.3+) along
  # with GPL-2.0+
  s.licenses = %w(GPL-2.0+ Nonstandard)
end
