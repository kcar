# -*- encoding: binary -*-
module Kcar
  autoload :Response, 'kcar/response'
end

require_relative 'kcar/version'
require_relative 'kcar/parser'
require 'kcar_ext'
