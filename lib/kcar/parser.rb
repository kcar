# -*- encoding: binary -*-
class Kcar::Parser

  # extract trailers that were set in the header object as
  # an array of arrays
  #
  #   parser.extract_trailers(hdr) =>
  #       [ [ 'Content-MD5', '1B2M2Y8AsgTpgAmY7PhCfg==' ] ]
  def extract_trailers(hdr)
    trailers = []

    if hdr.kind_of?(Array)
      t = {}

      # the HTTP spec (and our parser) guarantees trailers will appear
      # after the "Trailer" header is inserted in the array
      hdr.each do |key, value|
        if key =~ %r{\ATrailer\z}i
          value.split(/\s*,+\s*/).each do |k|
            t[k] = true
          end
        elsif !t.empty? && key =~ /\A(#{t.keys.join('|')})\z/i
          k = $1
          trailers.concat(value.split(/\n+/).map! { |v| [ k, v ] })
        end
      end
    elsif t = hdr['Trailer']
      t.split(/\s*[,\n]+\s*/).each do |k|
        value = hdr[k] or next
        trailers.concat(value.split(/\n+/).map! { |v| [ k, v ] })
      end
    end

    trailers
  end
end # module Kcar
