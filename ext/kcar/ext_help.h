#ifndef ext_help_h
#define ext_help_h

/* not all Ruby implementations support frozen objects (Rubinius does not) */
#if defined(OBJ_FROZEN)
#  define assert_frozen(f) assert(OBJ_FROZEN(f) && "unfrozen object")
#else
#  define assert_frozen(f) do {} while (0)
#endif /* !defined(OBJ_FROZEN) */

#if !defined(OFFT2NUM)
#  if SIZEOF_OFF_T == SIZEOF_LONG
#    define OFFT2NUM(n) LONG2NUM(n)
#  else
#    define OFFT2NUM(n) LL2NUM(n)
#  endif
#endif /* ! defined(OFFT2NUM) */

static inline int str_cstr_eq(VALUE val, const char *ptr, long len)
{
  return (RSTRING_LEN(val) == len && !memcmp(ptr, RSTRING_PTR(val), len));
}

#define STR_CSTR_EQ(val, const_str) \
  str_cstr_eq(val, const_str, sizeof(const_str) - 1)

static int cstr_case_eq(const char *a, long alen, const char *b, long blen)
{
  if (alen == blen) {
    for (; blen--; ++a, ++b) {
      if ((*a == *b) || ((*a >= 'A' && *a <= 'Z') && (*a | 0x20) == *b))
        continue;
      return 0;
    }
    return 1;
  }
  return 0;
}

/* strcasecmp isn't locale independent */
static int str_cstr_case_eq(VALUE val, const char *ptr, long len)
{
  return cstr_case_eq(RSTRING_PTR(val), RSTRING_LEN(val), ptr, len);
}

#define STR_CSTR_CASE_EQ(val, const_str) \
  str_cstr_case_eq(val, const_str, sizeof(const_str) - 1)
#define CSTR_CASE_EQ(ptr, len, const_str) \
  cstr_case_eq(ptr, len, const_str, sizeof(const_str) - 1)

#endif /* ext_help_h */
